package Model;

public class Player {

    private int x;
    private int y;
    private int w;
    private int h;


    public int getW() {
        return 30;
    }

    public int getH() {
        return 20;
    }

    public Player(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void move(int dx, int dy) {
        if ((dx < 0) && (x < 21)) {
            return;
        }
        if ((dx > 0) && (x > 970)) {
            return;
        }
        this.x += dx;
        this.y += dy;
    }

    public void moveto(int x, int y) {
        this.x = x;
        this.y = y;

    }
}
