package Model;

import java.util.LinkedList;
import java.util.List;

public class Model {

    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;

    public Boolean collision = false;
    private List<Car> cars = new LinkedList<>();
    private Player player;

    public void reset() {
        cars.clear();


        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));
        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));
        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));
        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));
        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));
        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));


        this.collision = false;

    }

    public Model(Boolean collision) {
        this.collision = collision;
    }

    public Player getPlayer() {
        return player;
    }

    public Model() {
        this.player = new Player(500, 550);

        this.cars.add(new Car(10, 0.1));
        this.cars.add(new Car(10, 0.12));
        this.cars.add(new Car(10, 0.15));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));

        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));
        this.cars.add(new Car(10, 0.19));
        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));
        this.cars.add(new Car(10, 0.19));
        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));
        this.cars.add(new Car(10, 0.19));
        this.cars.add(new Car(10, 0.03));
        this.cars.add(new Car(10, 0.17));
        this.cars.add(new Car(10, 0.18));
        this.cars.add(new Car(10, 0.19));
        this.cars.add(new Car(10, 0.03));


    }


    public List<Car> getCars() {

        return cars;
    }

    public void update(long elapsedTime) {
        if (!collision) {

            for (Car car : cars) {
                car.update(elapsedTime);
            }

            for (Car car : cars) {

                double dx = Math.abs(player.getX() - car.getX());
                double dy = Math.abs(player.getY() - car.getY());
                double w = player.getW() / 2 + car.getW() / 2;
                double h = player.getH() / 2 + car.getH() / 2;


                if (dx <= w && dy <= h) {
                    player.moveto(500, 550);
                    this.collision = true;

                }


            }

        }
    }


}



