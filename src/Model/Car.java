package Model;

public class Car {

    private double x;
    private double y;
    private double speedX;

    public Car(double y, double speedX) {
        this.x = Math.random() * 1000;
        this.y = y;
        this.speedX = speedX;
    }

    public void update(long elapsedTime) {

        this.y = (int) (this.y + elapsedTime * this.speedX);
        this.y = Math.round(this.y + elapsedTime * this.speedX);

        if (this.x > Model.WIDTH) {
            this.x = 0;

        } else if (this.y > Model.HEIGHT) {
            this.y = 0;
            this.x = Math.random() * 1000;
        }
    }


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getH() {
        return 40;
    }

    public double getW() {
        return 50;
    }


}

