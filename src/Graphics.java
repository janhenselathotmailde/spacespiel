import Model.Car;
import Model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class Graphics {

    private Model model;
    private GraphicsContext gc;


    Image iconMonster = new Image("monster.png");
    Image iconSpieler = new Image("superhero.png");
    Image iconGameOver = new Image("game-over.png");
    Image hintergrund = new Image("pia23182.jpg");


    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;

    }

    public void draw() {


        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.drawImage(hintergrund, 0, 0, Model.WIDTH, Model.HEIGHT);
        //
        gc.closePath();
        gc.setFill(Color.DARKORANGE);

        for (Car car : this.model.getCars()) {
            gc.drawImage(iconMonster, car.getX(), car.getY(), 50, 50);
        }
        gc.setFill(Color.AQUAMARINE);

        gc.drawImage(iconSpieler, model.getPlayer().getX() - 15,
                model.getPlayer().getY() - 15, 50, 50);


        if (model.collision == true) {
            gc.drawImage(iconGameOver, 300, 200, 250, 250);

        }
    }

    }



