import Model.Model;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;

public class InputHandler {
    private Model model;


    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode key) {

        if (key == KeyCode.RIGHT) {
            model.getPlayer().move(40, 0);
        } else if (key == KeyCode.LEFT) {
            model.getPlayer().move(-40, 0);
        }

        if (key == KeyCode.ESCAPE) {
            model.reset();
        }
        if (key == KeyCode.UP) {
            model.getPlayer().move(0, 30);

        }
        if (key == KeyCode.DOWN) {
            model.getPlayer().move(0, -30);
        }

    }

    public void mouseListener(MouseEvent e) {


    }
}


